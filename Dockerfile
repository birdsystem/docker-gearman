FROM artefactual/gearmand:1.1.18-alpine

ENV MYSQL_TABLE=${MYSQL_TABLE:-gearman_queue}

#EXPOSE 4730

CMD gearmand \
    --threads=0 \
    --log-file=stderr \
    --round-robin \
    --keepalive \
    --keepalive-idle=60 \
    --keepalive-interval=10 \
    --keepalive-count=3 \
    --queue-type=mysql \
    --mysql-host=${MYSQL_HOST} \
    --mysql-user=${MYSQL_USER} \
    --mysql-password=${MYSQL_PASSWORD} \
    --mysql-db=${MYSQL_DB} \
    --mysql-table=${MYSQL_TABLE} \
    --mysql-port=3306